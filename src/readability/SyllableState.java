package readability;

import java.util.ArrayList;

import syllableCounter.WordCounter;
/**
 * This State count the Syllable from ArrayList and return as integer.
 * @author Thanawit Gerdprasert.
 *
 */
public class SyllableState implements State{

	private int counter;
	
	public SyllableState(){
		counter =0;
	}
	@Override
	public int performWord(ArrayList<String> list) {
		WordCounter wordCount = new WordCounter();
		for(int i =0 ; i<list.size();i++)
		{
			counter += wordCount.countSyllables(list.get(i));
		}
		return counter;
	}

}
