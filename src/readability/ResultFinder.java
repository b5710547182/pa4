package readability;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * This class is used to compute the result of all count that will be shown.
 * @author Thanawit Gerdprasert
 *
 */
public class ResultFinder {

	private int sentenceCount;
	private int syllableCount;
	private int wordCount;
	private double fleshCount;
	private State state;

	/**
	 * initialize all information in this class.
	 */
	public ResultFinder(){
		sentenceCount =0; 
		setSyllableCount(0);
		setWordCount(0);
		setFleshCount(0);
	}

	/**
	 * calculate all the resource from given URL or File.
	 * @param name of the URL or File that will be used.
	 */
	public void calculateResult(String name){
		InputStream webURL;
		Scanner scanner = null;

		try{
			URL url = new URL(name);
			webURL = url.openStream();
			scanner = new Scanner(webURL);

		}catch(Exception e){
			try{
				File file = new File(name);
				scanner = new Scanner(file);
			}catch ( Exception e2 ) {
				
			}
		}
		ArrayList<String> texts = new ArrayList<String>();
		while(scanner.hasNext()) {
			texts.add(scanner.next());
		}
		setState(new SentenceState());
		sentenceCount = state.performWord(texts);
		setState(new SyllableState());
		setSyllableCount(state.performWord(texts));
		setState(new WordState());
		setWordCount(state.performWord(texts));
		setFleshCount(findIndex());
	}

	/**
	 * get the name of the file and print it to the console.
	 * @param name of the file
	 */
	public void printInConsole(String name){
		System.out.print(String.format("%-30s", "Filename: "));
		System.out.println(String.format("%s",name));
		System.out.print(String.format("%-30s", "Number of Syllables: "));
		System.out.println(String.format("%s", getSyllableCount()));
		System.out.print(String.format("%-30s", "Number of Words: "));
		System.out.println(String.format("%s", getWordCount()));
		System.out.print(String.format("%-30s", "Number of Sentences: "));
		System.out.println(String.format("%s", sentenceCount));
		System.out.print(String.format("%-30s", "Flesch Index: "));
		System.out.println(String.format("%.2f", getFleshCount()));
		System.out.print(String.format("%-30s", "Readability: "));
		System.out.println(String.format("%s", readLevel(getFleshCount())));
	}
	
	/**
	 * get the same of file and show result in the GUI.
	 * @param name of the file or URL.
	 */
	public void printInGUI(String name){
		FileCounterUI ui = FileCounterUI.getInstance();
		String resultName = String.format("%-30s%s\n" , "Filename: ", name);
		String resultSyllable = String.format("%-30s%s\n" , "Number of Syllables: ", syllableCount);
		String resultSentence = String.format("%-30s%s\n" , "Number of Sentences: ", sentenceCount);
		String resultIndex = String.format("%-30s%.2f\n" , "Flesch Index: ", fleshCount);
		String resultReadabi = String.format("%-30s%s\n" , "Readability: ", readLevel(getFleshCount()));
		ui.getResult().setText(String.format("%s",resultName+resultSyllable+resultSentence+resultIndex+resultReadabi));
		
	}
	/**
	 * calcalate the read level of the File or URL.
	 * @param counter of the Flesch index
	 * @return String describe the dificulty of read level.
	 */
	public String readLevel (double counter){
		if(counter>100){
			return "4th grade student";
		}
		else if(counter>90){
			return "5th grade student";
		}
		else if(counter>80){
			return "6th grade student";
		}
		else if(counter>70){
			return "7th grade student";
		}
		else if(counter>65){
			return "8th grade student";
		}
		else if(counter>60){
			return "9th grade student";
		}
		else if(counter>50){
			return "High school student";
		}
		else if(counter>30){
			return "College student";
		}
		else if(counter>=0){
			return "College graduate";
		}
		else
			return "Advance degree graduate";
	}

	/**
	 * set the current state.
	 * @param state that will be set to.
	 */
	public void setState(State state){
		this.state = state ;  
	}

	/**
	 * calculate the flesch index
	 * @return the amount of flesh index
	 */
	public double findIndex() {
		double index = 206.835 - 84.6*(getSyllableCount()/ getWordCount())-1.015*(getWordCount() / sentenceCount );
		return index;
	}

	/**
	 * return the count of syllable.
	 * @return syllable count.
	 */
	public int getSyllableCount() {
		return syllableCount;
	}

	/**
	 * set the syllable count to be same as parameter.
	 * @param syllableCount that will be set to.
	 */
	public void setSyllableCount(int syllableCount) {
		this.syllableCount = syllableCount;
	}

	/**
	 * return word count total word.
	 * @return word count.
	 */
	public int getWordCount() {
		return wordCount;
	}

	/**
	 * set the word count to be same as parameter.
	 * @param wordCount that will be change to.
	 */
	public void setWordCount(int wordCount) {
		this.wordCount = wordCount;
	}
/**
 * get the flesh count from class.
 * @return the flesh count.
 */
	public double getFleshCount() {
		return fleshCount;
	}
/**
 * set the flesh count to be same as other.
 * @param fleshCount that will be change to.
 */
	public void setFleshCount(double fleshCount) {
		this.fleshCount = fleshCount;
	}
	

}
