package readability;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * This State count total word from each class and return as integer.
 * @author Thanawit Gerdprasert.
 *
 */
public class WordState implements State{

	private int counter;
	public WordState(){
		counter =0 ;
	}
	@Override
	public int performWord(ArrayList<String> list) {
		Iterator<String> iter = list.iterator();
		while(iter.hasNext()){
			iter.next();
			counter++;
		}
		return counter;
	}

}
