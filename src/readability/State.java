package readability;

import java.util.ArrayList;
/**
 * State for the class that will calculate total word.
 * @author Thanawit Gerdprasert.
 *
 */
public interface State {
	/**
	 * Sum the word from list and return as integer.
	 * @param list of the word that received.
	 * @return the total word from the calculation.
	 */
	public int performWord(ArrayList<String> list);
}
