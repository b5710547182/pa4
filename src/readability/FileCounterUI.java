package readability;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
/**
 * Singleton GUI for the Flesch Index counter that can brouwse the file and find the result.
 * @author Thanawit Gerdprasert
 *
 */
public class FileCounterUI extends JFrame{
	private static FileCounterUI ui;
	private JTextField fileName;
	private JTextArea result;


	private FileCounterUI(){
		this.initComponent();
		this.run();
	}
	/**
	 * Create the GUI if it's not null, else return the GUI.
	 * @return the singleton of this GUI.
	 */
	public static FileCounterUI getInstance()
	{
		if(ui == null)
		{
			ui = new FileCounterUI();
		}
		return ui;
	}
	/**
	 * set visible for this GUI.
	 */
	public void run(){
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	/**
	 * initialize all GUI component and adding ActionListener for all button.
	 */
	public void initComponent(){
		super.setTitle("File Counter");

		super.setSize(600, 200);
		JPanel box = new JPanel();
		Container upper = new Container();
		Container lower = new Container();

		box.setLayout(new BoxLayout(box, BoxLayout.Y_AXIS));

		upper.setLayout(new FlowLayout());
		lower.setLayout(new FlowLayout());

		JLabel text = new JLabel("File or URL name : ");
		fileName = new JTextField(15);
		JButton browseButton = new JButton("Browse");
		JButton countButton = new JButton("Count");
		JButton clearButton = new JButton("Clear");

		upper.add(text);
		upper.add(fileName);
		upper.add(browseButton);
		upper.add(countButton);
		upper.add(clearButton);
		result = new JTextArea(10,50);
		result.setText("");
		result.add(new JScrollPane());

		lower.add(result);

		box.add(upper);
		box.add(lower);

		this.add(box);

		clearButton.addActionListener(new ClearButton());
		countButton.addActionListener(new CountButton());
		browseButton.addActionListener(new BrowseButton());
	}
	/**
	 * 
	 * @return
	 */
	public JTextArea getResult(){
		return result;
	}
	public JTextField getFileName(){
		return this.fileName;
	}

	public static void main(String[] args) {
		FileCounterUI ui = FileCounterUI.getInstance();
		ui.run();
	}

}


/**
 * Action Listener used to Browse and get the directory of the file
 * @author Thanawit Gerdprasert
 *
 */
class BrowseButton implements ActionListener{

	private JButton open;
	private File selectedFile;

	public void actionPerformed(ActionEvent e) {
		JFileChooser myChooser;
			myChooser = new JFileChooser();
			if (myChooser.showOpenDialog(FileCounterUI.getInstance())== JFileChooser.APPROVE_OPTION) {
				selectedFile = myChooser.getSelectedFile();
				FileCounterUI.getInstance().getFileName().setText(selectedFile.getAbsolutePath());
			}
		
	}
}
/**
 * ActionListener for the Clear Button
 * @author Thanawit Gerdprasert.
 *
 */
	class ClearButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			FileCounterUI ui = FileCounterUI.getInstance();
			ui.getResult().setText("");;

		}

	}
	/**
	 * ActionListener for CountButton that show the data of the
	 * @author Thanawit Gerdprasert.
	 *
	 */
	class CountButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			FileCounterUI ui = null;
			try{
				ResultFinder finder = new ResultFinder();
				ui = FileCounterUI.getInstance();
				finder.calculateResult(ui.getFileName().getText());
				finder.printInGUI(ui.getFileName().getText());
			}
			catch(Exception e1)
			{
				JOptionPane temp = new JOptionPane();
				JOptionPane.showMessageDialog(ui, "Please input valid File");

			}

		}


	}