package readability;
/**
 * Application class for both GUI stype and command line style.
 * @author Thanawit Gerdprasert
 *
 */
public class Readability {

	/**
	 * Main class that execute the class.
	 * @param args that receive from the command line, can be file or URL.
	 */
	public static void main(String[] args) {
		if(args.length <= 0 )
		{
			FileCounterUI frame = FileCounterUI.getInstance();
			frame.run();
		}
		else{
			ResultFinder op = new ResultFinder();
			op.calculateResult(args[0]);
			op.printInConsole(args[0]);
		}
	}
}