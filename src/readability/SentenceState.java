package readability;

import java.util.ArrayList;
/**
 * This class implement State and will count the number of the sentence.
 * @author Thanawit Gerdprasert.
 *
 */
public class SentenceState implements State{

	private int counter;
	public SentenceState(){
		counter =0;
	}
	
	
	@Override
	public int performWord(ArrayList<String> list) {
		for(int i =0 ; i<list.size();i++)
		{
			if(list.get(i).contains(".")||list.get(i).contains("?")||list.get(i).contains("!")||list.get(i).contains(":"))
			{
				counter++;
			}
		}
		return counter;
		
		
	}

}
