package syllableCounter;


/**
 * E-First State that can check unit and also update the Syllable counter 
 * @author Thanawit Gerdprasert
 *
 */
public class E_first extends CheckingUnit implements State{

	@Override
	public void handleChar(char word) {
		

		if(super.isVowel(word))
		{
				WordCounter.syllables++;
				WordCounter.state = new Vowel();
		}
		else if(super.isLetter(word))
		{
			WordCounter.state = new Consonant();
		}
		else
		{
			WordCounter.state = new NonWord();
		}
	}


}
