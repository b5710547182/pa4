package syllableCounter;

/**
 * the State pattern for the Counter class
 * @author Thanawit Gerdprasert.
 *
 */
public interface State {

	/**
	 * handle the char and add and update the counter.
	 * @param word that will be check and update or not.
	 */
	public void handleChar(char word);
	
}
