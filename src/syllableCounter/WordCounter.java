package syllableCounter;

/**
 * WordCounter that can find all syllable and also the amount of word
 * @author Thanawit Gerdprasert
 *
 */
public class WordCounter extends CheckingUnit {

	public static boolean havingFirstE = false;
	public static State state;
	public static int syllables;
	public static boolean vowelFound = false;
	/**
	 * initialize the data.
	 */
	public WordCounter()
	{
		WordCounter.syllables =0;
		WordCounter.havingFirstE = false;
		WordCounter.state = new Start();
	}
	/**
	 * count and update the syllable.
	 * @param word that received and find t
	 * @return the count of syllables after calculation.
	 */
	public int countSyllables(String word)
	{
		WordCounter.syllables =0;
		WordCounter.vowelFound = false;
		int checker = 0;
		boolean hasLastE = false;
		int duplicateWord = 0;
		char lastChar = word.charAt(word.length()-1);
		
		
		//check first E case
		for(int i =0 ; i < word.length(); i++)
		{
			if(word.charAt(i)=='e'||word.charAt(i)=='E')
			{
				checker++;
			}
			if(super.isVowel(word.charAt(i)))
			{
				duplicateWord ++;
			}
			
		}
		//check last E
		if(lastChar=='e'||lastChar=='E')
		{
			hasLastE = true;
		}
		
		// added when have 1 E
		if(checker==1)
		{
			WordCounter.syllables++;
		}
		//normal handler char loop
		for(int i =0 ; i<word.length();i++)
		{
			WordCounter.state.handleChar(word.charAt(i));
		}
		//decrease count when have E as both first and last
		if(hasLastE&&WordCounter.havingFirstE)
		{
			WordCounter.syllables--;
		}
		//increase count when have E as the vowel
		if(WordCounter.vowelFound ==false && hasLastE==true)
		{
			WordCounter.syllables++;
		}
		//all verb case
		if(duplicateWord == word.length()-1)
		{
			WordCounter.syllables =1;
		}
		//non word start case
		if(super.isLetter(word.charAt(0))== false && super.isVowel(word.charAt(0))== false)
		{
			WordCounter.syllables = 0;
		}
		// non word end case
		if(super.isLetter(lastChar)==false&&super.isVowel(lastChar)==false)
		{
			WordCounter.syllables = 0;
		}
		
		
		
		
		WordCounter.havingFirstE = false;
		return WordCounter.syllables;
	}
	
}
