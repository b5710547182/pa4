package syllableCounter;


/**
 * Syllable State that can check unit and also update the Syllable counter 
 * @author Thanawit Gerdprasert
 *
 */
public class Vowel extends CheckingUnit implements State{

	@Override
	public void handleChar(char word) {
		if((word=='e'||word=='E')&&WordCounter.havingFirstE==false)
		{
			WordCounter.havingFirstE = true;
			WordCounter.state = new E_first();
		}
		else if(super.isVowel(word))
		{
			WordCounter.state = new Vowel();
		}
		else if(super.isLetter(word)||word=='y'||word=='Y')
		{
			WordCounter.state = new Consonant();
		}
		else
		{
			WordCounter.state = new NonWord();
		}
		
	}


}
