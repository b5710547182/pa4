package syllableCounter;

/**
 * Check main class that check the unit that whether it contain vowel or letter or not.
 * @author Thanawit Gerdprasert.
 *
 */
public class CheckingUnit {

	/**
	 * Check if character is vowel or not.
	 * @param c character that want to check.
	 * @return true if it is vowel else return false.
	 */
	public boolean isVowel(char c)
	{
		return "AEIOUaeiou".indexOf(c)>=0;
	}
	/**
	 * Check if character is letter or not.
	 * @param c character that want to check.
	 * @return true if it is character else return false.
	 */
	public boolean isLetter(char c )
	{
		return Character.isLetter(c);
	}
	
	
}
